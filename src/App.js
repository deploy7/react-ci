import React from 'react';
// import logo from './logo.svg';
import './App.css';
import {BrowserRouter,Link,Route,Switch} from "react-router-dom"
import Table from './components/table';
import Form from "./components/form";
import Api from "./components/json"
class App extends React.Component{
 
  render(){
    console.log(this.props.match)
    return (
      <>
      <BrowserRouter basename="/react-ci">
      <nav className="navbar navbar-dark bg-dark navbar-expand-md">
        <Link to="/" className="navbar-brand">App</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-collapse">
        {/* <button class="navbar-toggler" type="button" > */}
    <span className="navbar-toggler-icon"></span>
 </button>
 <div className="collapse navbar-collapse" id="nav-collapse">
    <ul className="navbar-nav ml-auto">
        <li className="nav-item">
        <Link className="nav-link" to="/form">Form</Link>
         </li>
         <li className="nav-item">
         <Link className="nav-link" to="/table">Table</Link>
          </li>
          <li className="nav-item">
         <Link className="nav-link" to="/json">API</Link>
          </li>
        
     </ul>
</div>

      </nav>
    

<Switch>
  
      <Route exact path="/table" component={Table}/>
      <Route exact path="/form" component={Form}/>
      <Route exact path="/json" component={Api}/>
      
      <Route exact path="/" render={()=>{
    return  (<div className="container">
    <div className="row">
        <div className="col-6 offset-3 text-center">
          <br/><br/>
          <div className="highlight">
          <h2>Click on Form to go to Form Page</h2>
          <h2>Click on Table to go to Table Page</h2>

          </div>
         
        </div>

      </div>
      
      
      </div>)
  }}></Route>
      </Switch>
      </BrowserRouter>
      
      </>
    )
  }

  
}

export default App;
