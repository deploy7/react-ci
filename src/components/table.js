import React, { Component } from 'react';

class table extends Component {
    constructor(){
        super();
        this.state={
            array:[{name:"Mi",price:13000},{name:"Oppo",price:12000},{name:"Huawei",price:35000}],
            form:{
                Mi:0,
                Oppo:0,
                Huawei:0
                
            },
            totalPrice:0
        }
    }
    change1=(event)=>{
       
        let name=event.target.name;
        let value=event.target.value;
        console.log(value*12)
        let {form}=this.state;
        this.setState({form:{...form,[name]:value}},()=>{
            this.calculateValue();
        });



    }
    calculateValue=()=>{
        let quantity=this.state.form;
        let price1=0;
        this.state.array.forEach((data)=>{
             price1+=data.price*quantity[data.name];
            return price1

        })
        this.setState({totalPrice:price1})
 }
    getTable=()=>{
        let arra=this.state.array.map((data,i)=>{
            return (<tr key={i}>
                <td>{data.name}</td>
                <td>
                    {data.price}
                </td>
                <td><input type="number" className="form-control" name={data.name} onChange={this.change1}></input></td>

            </tr>

            )

        })
        return arra;
    }
    render() {
        console.log(this.state)
        return (
            <>
            <br/>
            <table className="table table-bordered">
                <thead className="text-center">
                    <tr><th>Product Name</th>
                    <th>Product Price</th>
                    <th>Product Quantity</th></tr>
                   
                </thead>
                <tbody>
                    {this.getTable()}
                </tbody>

            </table>
        {this.state.totalPrice===0?null:<center><span>Total Amount to be Paid: {this.state.totalPrice}</span></center>}</>
            
        );
    }
}

export default table;